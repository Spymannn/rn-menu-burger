import * as React from "react";
import { View, StyleSheet } from "react-native";
import { DrawerContentScrollView, DrawerItem } from "@react-navigation/drawer";
import {
  Avatar,
  Title,
  Caption,
  Paragraph,
  Drawer,
  Text,
  TouchableRipple,
  Switch,
} from "react-native-paper";
import { Ionicons } from "@expo/vector-icons";

export function Sidebar(props: any) {
  const [isDarkTheme, setIsDarkTheme] = React.useState(false);
  return (
    <View style={{ flex: 1 }}>
      <DrawerContentScrollView {...props}>
        <View style={styles.drawerContent}>
          <View style={styles.userInfoSection}>
            <View
              style={styles.avatarWrapper}
              onPress={() => console.log("go to profile settings")}
            >
              <Avatar.Image
                source={{
                  uri:
                    "https://vignette.wikia.nocookie.net/war-of-east-hunter-x-hunter/images/2/2d/Gon_Freecss.png/revision/latest?cb=20190811191852",
                }}
                size={50}
              />
              <View style={styles.userInfoWrapper}>
                <Title style={styles.title}>Samir Hanini</Title>
                <Caption style={styles.caption}>@Spymannn</Caption>
              </View>
            </View>
            <View style={styles.row}>
              <View>
                <Paragraph>Swing and Charleston dancer</Paragraph>
              </View>
            </View>
          </View>
          <Drawer.Section style={styles.drawerSection}>
            <DrawerItem
              icon={({ color, size }: { color: string; size: number }) => (
                <Ionicons name="md-people" color={color} size={size} />
              )}
              label="Swing"
              onPress={() => props.navigation.navigate("Swing")}
            />
            <DrawerItem
              icon={({ color, size }: { color: string; size: number }) => (
                <Ionicons name="md-person-add" color={color} size={size} />
              )}
              label="Charleston"
              onPress={() => props.navigation.navigate("Charleston")}
            />
            <DrawerItem
              icon={({ color, size }: { color: string; size: number }) => (
                <Ionicons name="md-person" color={color} size={size} />
              )}
              label="Solo Jazz"
              onPress={() => props.navigation.navigate("SoloJazz")}
            />
            <DrawerItem
              icon={({ color, size }: { color: string; size: number }) => (
                <Ionicons name="md-glasses" color={color} size={size} />
              )}
              label="Balboa"
              onPress={() => props.navigation.navigate("Balboa")}
            />
            <DrawerItem
              icon={({ color, size }: { color: string; size: number }) => (
                <Ionicons name="md-walk" color={color} size={size} />
              )}
              label="Tap"
              onPress={() => props.navigation.navigate("Tap")}
            />
          </Drawer.Section>
          <Drawer.Section>
            <DrawerItem
              icon={({ color, size }: { color: string; size: number }) => (
                <Ionicons name="md-musical-notes" color={color} size={size} />
              )}
              label="Music"
              onPress={() => props.navigation.navigate("Music")}
            />
          </Drawer.Section>
          <Drawer.Section title="Preferences">
            <TouchableRipple onPress={() => setIsDarkTheme(!isDarkTheme)}>
              <View style={styles.preference}>
                <Text>Dark Theme</Text>
                <View pointerEvents="none">
                  <Switch value={isDarkTheme} />
                </View>
              </View>
            </TouchableRipple>
          </Drawer.Section>
        </View>
      </DrawerContentScrollView>
      <Drawer.Section style={styles.bottomDrawerSection}>
        <DrawerItem
          icon={({ color, size }: { color: string; size: number }) => (
            <Ionicons name="md-exit" color={color} size={size} />
          )}
          label="Logout"
          onPress={() => console.log("signout")}
        />
      </Drawer.Section>
    </View>
  );
}

const styles = StyleSheet.create({
  drawerContent: {
    flex: 1,
  },
  userInfoSection: {
    paddingLeft: 20,
  },
  avatarWrapper: {
    flexDirection: "row",
    marginTop: 15,
  },
  userInfoWrapper: {
    marginLeft: 15,
    flexDirection: "column",
  },
  title: {
    fontSize: 16,
    marginTop: 3,
    fontWeight: "bold",
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
  },
  row: {
    marginTop: 20,
    flexDirection: "row",
    alignItems: "center",
  },
  section: {
    flexDirection: "row",
    alignItems: "center",
    marginRight: 15,
  },
  paragraph: {
    fontWeight: "bold",
    marginRight: 3,
  },
  drawerSection: {
    marginTop: 15,
  },
  bottomDrawerSection: {
    marginBottom: 15,
    borderTopColor: "#f4f4f4",
    borderTopWidth: 1,
  },
  preference: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 12,
    paddingHorizontal: 16,
  },
});
