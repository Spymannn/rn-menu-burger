import React from "react";
import Screen from "./Screen";

export const SwingScreen = (navigation: any) => (
  <Screen navigation={navigation} pageName="Swing" />
);
export const CharlestonScreen = (navigation: any) => (
  <Screen navigation={navigation} pageName="Charleston" />
);
export const SoloJazzScreen = (navigation: any) => (
  <Screen navigation={navigation} pageName="Solo Jazz" />
);
export const BalboaScreen = (navigation: any) => (
  <Screen navigation={navigation} pageName="Balboa" />
);
export const TapScreen = (navigation: any) => (
  <Screen navigation={navigation} pageName="Tap" />
);
export const ProfileScreen = (navigation: any) => (
  <Screen navigation={navigation} pageName="Profile" />
);
export const SignOutScreen = (navigation: any) => (
  <Screen navigation={navigation} pageName="SignOut" />
);
