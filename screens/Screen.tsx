import React from "react";
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
} from "react-native";
import { FontAwesome5 } from "@expo/vector-icons";

export default function Screen({navigation, pageName}: {navigation: any, pageName: String}) {
    return (
        <View style={styles.container}>
            <SafeAreaView style={{ flex: 1 }}>
            <TouchableOpacity style={styles.touchableOpacity} onPress={navigation.navigation.openDrawer}>
                <FontAwesome5 name="bars" size={24} color="#161924"></FontAwesome5>
            </TouchableOpacity>
            <View style={styles.title}>
                <Text style={styles.text}> {pageName} Screen </Text>
            </View>
            </SafeAreaView>
        </View>
    )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  text: {
    color: "#161924",
    fontSize: 20,
    fontWeight: "500",
  },
  touchableOpacity: {
    alignItems: "flex-start",
    margin: 16
  },
  title: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
});
