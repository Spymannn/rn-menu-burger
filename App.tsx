import React from "react";

import { NavigationContainer } from "@react-navigation/native";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { View } from "react-native";

import {
  SwingScreen,
  CharlestonScreen,
  SoloJazzScreen,
  BalboaScreen,
  TapScreen,
  ProfileScreen,
  SignOutScreen,
} from "./screens";

import { Sidebar } from "./components/SideBar";

const DrawerNavigator = createDrawerNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <DrawerNavigator.Navigator
        initialRouteName="Swing"
        drawerContent={(props) => <Sidebar {...props} />}
      >
        <DrawerNavigator.Screen name="Swing" component={SwingScreen} />
        <DrawerNavigator.Screen
          name="Charleston"
          component={CharlestonScreen}
        />
        <DrawerNavigator.Screen name="SoloJazz" component={SoloJazzScreen} />
        <DrawerNavigator.Screen name="Balboa" component={BalboaScreen} />
        <DrawerNavigator.Screen name="Tap" component={TapScreen} />
        <DrawerNavigator.Screen name="Music" component={ProfileScreen} />
        <DrawerNavigator.Screen name="Profile" component={ProfileScreen} />
        <DrawerNavigator.Screen name="Signout" component={SignOutScreen} />
      </DrawerNavigator.Navigator>
    </NavigationContainer>
  );
}
